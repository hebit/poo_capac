# Capacitação POO

>Ajudar pessoas à lidar com objetos (e isso não é uma crítica social)  

Olar pessoas, preparados para continuar nessa jornada do aprendizado? 😜. Bora lá então!  

Antes de tudo, nossa capacitação foi/será realizada com PHP, então qualquer dúvida relacionado com a linguagem sugerismo que acesse a [documentação](https://www.php.net/docs.php) ou então peça por ajuda ❤!

*  [Conceito de objeto](#conceito-de-objeto)
*  [Criando seu primeiro objeto](#criando-seu-primeiro-objeto)
*  [Manipulando objetos](#manipulando-objetos)
*  [Visibilidade](#visibilidade)
    *   [Getters e Setters](#getters-e-setters)
*  

## Conceito de objeto

No seu dia-a-dia, querendo ou não, há inumeros objetos com diversas características e funcionalidades. Você até poderia imaginá-los como variavéis, armazenando várias informações sobre o objeto, mas será que todos objetos tem o mesmo número de característica? Todos tem as mesmas funcionalidades? E uma mesma funcionalidade é realizado da mesma forma por objetos diferentes? 🤔  
Espero não ter te deixado confuso, mas se eu deixei saiba que Orientação à Objetos deixará tudo mais explicativo 😎.  

Digamos que tentássemos abstrair objetos dividindo suas características (atributos) e funcionalidades (métodos):  
*ex.: [**Pessoa**]*   

|atributos  |métodos             |
|---        |---                 |
|nome|*abraçar*     |
|altura        |*falar*|
|peso        |*cantar* |
|idade        |*estudar*      |
|rg       |*fechar issues 🔝*   |

Ok, agora chega de teoria, provavelmente tu gosta mais é da prática


## Criando seu primeiro objeto
Para declararmos a estrutura de básica de um objeto usamos a palavra reservada *class* e em seguida nomeamos nossa classe. Dentro da classe podemos ter atributos e métodos como estavamos definindo acima (ignore o **public** por enquanto).

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
    
        public function sayHello(){
            echo "Hello :)";
        }
    }
    
```
Prontinho, nosso esqueleto já está pronto!

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
    
        public function sayHello(){
            echo "Hello :)";
        }
    }
    
    $pablo = new Person;
    
```

Você pode estar achando estranho não ter uma declaração explícita como haviam em variáveis (`var = 10`), mas essa "declaração" que acabamos de fazer chama-se de instanciar, ou seja, criamos um objeto segundo os moldes da nossa classe. Esse objeto agora tem nome, altura (`$name`, `$height`) e ainda pode falar 🤯 ("[*muito Black Mirror*](https://www.youtube.com/watch?v=aHPVJWgfUA8)").  

## Manipulando objetos
Agora que temos nosso objeto instanciado, podemos definir seus atributos. Perceba que até agora não definimos a altura de pablo (nem mesmo o nome 👀),então vamos fazer isso agora mesmo! Para isso usamos `->` para acessar os atributos e métodos de nosso objeto.

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
    
        public function sayHello(){
            echo "Hello :)";
        }
    }
    
    $pablo = new Person;
    
    $pablo->height = 1.80;
    $pablo->name = "Pablo Escobar";
    
    $pablo->sayHello();
    
```

Done! Definimos o nome e altura de `$pablo`, e ainda fizemos ele falar um oi.

## Visibilidade
Chegou a hora de ver o que dissemos na seção de [Criando seu primeiro objeto](#criando-seu-primeiro-objeto) pra ignorar, o tal do **public** (e **protected** e **private**). Essas três palavras referem-se à visibilidade de atributos e métodos de nossa classe. Imagine que quisersemos ter um atributo para o CPF da pessoa, será que o CPF deveria ser tão facilmente acessdo quanto as outras propriedades? 🤔. Bom, certamente não, e, para isso, temos o controle de visibilidade dentro de nossa classe.


| - | public | protected | private |
| --- |------ | --------- | ------- |
| pode ser acessada: | livremente  | apenas dentro da classe  | apenas dentro da classe  |
| pode ser herdada: | sim  |  sim  |  não  |  

Ok, mas o que seria acessar apenas dentro da classe? (mais pra frente veremos a questão da herança).

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
        
        private $cpf;
    
        public function sayHello(){
            echo "Hello :)";
        }
    }
    
    $pablo = new Person;
    
    $pablo->height = 1.80;
    $pablo->name = "Pablo Escobar";
    
    $pablo->cpf = 123; //Erro Fatal
    
    $pablo->sayHello();
    
```

O acesso restringido implica que só podemos usar determinado atributo ou função dentro do escopo da classe. De uma forma diferente, poderiamos mudar o valor do CPF por um método da classe:

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
        
        private $cpf;
    
        public function sayHello(){
            echo "Hello :)";
        }
        
        public function setCpf($cpf){
            $this->cpf = $cpf;
        }
    }
    
    $pablo = new Person;
    
    $pablo->height = 1.80;
    $pablo->name = "Pablo Escobar";
    
    $pablo->setCpf(123);
    
    $pablo->sayHello();
    
```
Para nos referir à algum atributo ou método dentro da classe usamos o `$this->atributo` ou `$this->metodo()`. Assim veja que não tivemos erros, conseguimos mudar o valor do CPF através de um método, manipulando através da classe e não por uma acesso direto no objeto instanciado.  

### Getters e Setters

Vamos olhar de novo o exemplo de pessoa:

```php
<?php 

    class Person {
        public $name;
        public $height;
        public $hobbies = [];
        
        private $cpf;
    
        public function sayHello(){
            echo "Hello :)";
        }
        
        public function setCpf($cpf){
            $this->cpf = $cpf;
        }
    }
    
    $pablo = new Person;
    
    $pablo->height = 1.80;
    $pablo->name = "Pablo Escobar";
    
    $pablo->setCpf(123);
    
    $pablo->sayHello();
    
```
Uma coisa importante de entender é que quando a propriedade está `private` nós não só ficamos impedidos de editar o valor dela, mas também de ver (ao menos da forma que fazíamos antes). Então o ideal para isso seria criar um método `getCpf()`, mais ou menos assim:

```php
...
    public function getCpf(){
        return $this->cpf;
    }
...
```

A ideia é fazer isso com basicamente todos atributos. O nome disso é **encapsulamento**.

Aí cê me pergunta:  
🤔: *Beleza, mas pra que eu vou usar isso se eu podia simplesmente usar daquela forma anterior, muito mais simples e com menos linhas, sendo menos trabalhoso?*

A resposta não é simples, infelizmente. Na verdade eu me perguntei isso todos os dias de minha vida até pouco tempo atrás 😖. A real é que encapsulamento permite várias coisas boas na sua vida.

Se ligue no pulo do gato:

1. Você consegue trabalhar com níveis de acesso diferentes pra `get` e `set`, podendo, por exemplo, fazer um `get` público e um `set` protected;
2. Você consegue fazer *validação* dos dados, como por exemplo só permitir que um `cpf` seja armazenado se for válido (lembra da atividade de js, jovi? 😬) ou retornar um `cpf` já formatado;
3. É o que geralmente é mais utilizado, então usar isso como padrão garante mais facilidade de leitura e manutenibilidade (pesquisei como escreve no google) do código;
4. Facilita o debugging com ferramentas próprias pra isso.

Enfim, essas são só algumas coisas que são vantajosas nesse uso. Mas nada disso é regra; muito pelo contrário, é uma Guerra Civil ™ na comunidade de programação. Mas por motivos do *item (3)*, vamos usar, tá bom?

Então recapitulando e trazendo algumas informações novas: 
- Encapsulamento é usar `private`, `public`, `protected`, `get` e `set`;
- O padrão de nomeação é `getNomeDoAtributo()` e `setNomeDoAtributo($atributo)`, vide `getNome()`, `setNome($nome)`;
- Usar isso é útil por vários motivos;
- Use isso, por favor;
- *(Segredo) Usar `get` e `set` não dá mais trabalho*.

Falando sério sobre o último ponto: quase todas IDEs permitem que você gere Getters e Setters automaticamente, então recorra a isso. No VSCode, a extensão que faz isso é a **"PHP Getters & Setters", por phproberto**.


